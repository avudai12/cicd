import { VPage } from './app.po';

describe('v App', () => {
  let page: VPage;

  beforeEach(() => {
    page = new VPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
